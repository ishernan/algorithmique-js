let userAges = [
    [
        'Jean_luc', 53
    ],
    [
        'Bond', 35
    ],
    [
        'Jocker', 75
    ]
];


let expectedResult = 'Jean-Luc 53 \nBond 35 \nJoker 75 \n';
let realResult = '';
for (let bigTableIndice = 0; bigTableIndice < userAges.length; bigTableIndice = bigTableIndice + 1) {
let user = userAges[bigTableIndice];
for (let smallTableIndice = 0; smallTableIndice < user.length; smallTableIndice = smallTableIndice + 1) {
realResult = realResult + user[smallTableIndice] + ' ';
}
realResult = realResult + '\n';
}
if (expectedResult === realResult) {
console.log(realResult);
} else {
console.log('Try again rookie');
}