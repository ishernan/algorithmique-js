import { User } from "../models/user";
import { Collection } from "./collection";

export class UserCollection extends Collection<User> {
    /**
     * @Override on reecrit la methode add
     * @param user
     */
    public add(user: User): void{
        let indexOfUserInCollection: number = -1; 

        indexOfUserInCollection = this.collection.findIndex(
            (userInCollection: User) => userInCollection.getId() === user.getId()
        ); 

        if (indexOfUserInCollection === -1) {
            super.add(user); 
        }
        
    }
}