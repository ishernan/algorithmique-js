import { UserCollection } from "../collection/user-collection";
import { User } from "../models/user";

export class Login {
    private repository: UserCollection;

    private userLogin!: string; 
    private userPassword!: string; 

    //Dependency Injection
    public constructor(repository: UserCollection) { //injection de la collection de users
        this.repository = repository; //on stocke la collection en repository
    }

    public setCredentials(login: string, password:string): void { //recuperer login et pass pour stocker dans la classe
        this.userLogin = login;
        this.userPassword = password; 
    }

    public process() : boolean { //méthode pour verifier les correspondances
        //recuperer users
        let allUsers : User [] = this.repository.all(); 

        //verifier les id et pass 
        let find: boolean = false; //on part du principe qu'on va pas les trouver

        for (let i:number = 0; i < allUsers.length; i++ ) { //parcourir la liste (on peut aussi utiliser un findIndex)
            const user: User = allUsers[i]; //recuperer les elements de la liste

            if (user.getId() === this.userLogin && user.getPassword() === this.userPassword){
                find = true; //la verification de coincidences
            }

        } 
        return find;           
    }        
}
