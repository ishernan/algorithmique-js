import { UserCollection } from "./src/collection/user-collection";
import { User } from "./src/models/user";
import { Login } from "./src/sevices/login"; 

class Main {
    public constructor() {
        const userCollection: UserCollection = new UserCollection(); 

        const user: User = new User();
        user.lastName = 'Aubert';
        user.firstName = 'Jean-Luc'; 
        user.setId('jlaubert');
        user.setPassword('admin'); 
        console.log(user.toString());

        userCollection.add(user);

        console.log(`Collection contains : ${userCollection.size()} item(s)`); 

        // const id: string = 'toto'; 
        // const password: string = 'titi'; 

        const id: string = 'jlaubert'; 
        const password: string = 'admin';

        const login: Login = new Login(userCollection); // la varieble "login" devient une instance de la classe Login
        login.setCredentials(id, password); //definit dans la classe Login l'id et le password
        const result = login.process();
        if (result) {
            console.log("Ok tu peux");
        }
        else {
            console.log("nope!")
        }

    }
}

//Load Main

new Main(); 