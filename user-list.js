class User {
    constructor(){
        this.name = '';
        this.firstName = '';
        this.id = '';
        this.password = '';

    }
}

/**
 * unique(user: User, list: User[])
 * @param user: User une variable de type user
 * @param list: User[] un tableau de User
 * @returns boolean
 */

function unique(user, list) {
    let yesYouCan = true; 
    for(let index=0; index < list.length; index = index + 1){
        if(list[index].id === user.id){
            yesYouCan = false;
        };
    }
    return  yesYouCan; 
    
}

/**
 * unique(user: User, list: User[])
 * @param {*} user: Some user variable
 * @param {*} list: A list of users
 * @returns Users[]
 */

function ajouter(user, list) {
    if(unique(user, list)){
        list.push(user)     
    }
    return list; 
}


/**
 * Début de l'application
 */
let listUsers = []; 

const user = new User();
user.name = 'Aubert';
user.firstName = 'Jean-Luc';
user.id = 'jlaubert';
user.password = 'admin';

listUsers = ajouter(user, listUsers); 

const isaac = new User()
isaac.name = 'isaac';
isaac.firstName = 'isaac-Luc';
isaac.id = 'isaac';
isaac.password = 'admin';

listUsers = ajouter(isaac, listUsers); 

//isaac.id = 'jlaubert'; 

console.log(`Items : ${listUsers.length}`); 

listUsers.forEach((user) => {
    console.log(`User : ${user.name} : ${user.id}`);
});


const generator = (length = 8) => {
    return Math.random().toString(16).substr(2, length);
  };

let password = 'admin';
let passReverse = password; 
let ajout = generator(20); 
password = password.concat(ajout); 

let changePass = password.split("").reverse().join('');

console.log(changePass);

let reversePass = changePass.split("").reverse().join('');
reversePass = reversePass.substr(0, passReverse.length);

console.log(reversePass);




// reversePass = nombre; 
// nombre = reversePass.length; 

// for(let i = 0; i <= 4; i = i + 1) {
    
//     return nombre[i]
        
//     }

// console.log(reversePass);


