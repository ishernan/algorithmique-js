let myName = ''; 
let yourName = '';
// Affectation de valeur Aubert à la variable myName
myName = 'Aubert'; 
yourName = 'Bond';

//Affichage du contenu de la variable muName
console.log(myName); //Expected output : Aubert

//Changer la valeur de myName par autre chose
myName = 'Autre chose'; 
console.log(myName); 

myName = yourName; 

let isMale = true; 
let hasDog = true; 

yourName = 'Superman'; 

console.log(`${myName} <=> ${yourName}`); 


let users = [
    'Aubert',
    'Abdel',
    'Nesibe',
    'Rainui',
    'Rémy'
]; 

//boucle non conditionnel
for(let index = 0; index <= 4; index = index + 1) {
    console.log(users[index]);
}

//boucle conditionnel
let indice = 0;
while (indice < users.length){
    console.log(users[indice]); 
    indice = indice + 1; 
}

//boucle conditionnel, à utiliser uniquement si besoin extrème, sinon à bannir
// index = 0;
// do {
//     console.log(users[index]);
//     index = index+1;
// }while (index < users.length)

//boolean
if(users[0] == 'Aubert') {
    console.log(`Salut ${users[3]}`);
}

if(users[3] == 'Aubert') {
    console.log(`Salut ${users[3]}`);
} else {
    console.log(`tu n'est pas Aubert, salut Rainui`);
}

let isTrue = 0; 
if(isTrue === false) {
    console.log('je suis vrai'); 
} else {
    console.log('je suis faux'); 
}

//ternaire
(users[0] === 'Aubert') ? 
    console.log('Okay JL') : 
    console.log('pas JL'); 

let isJL = users[0] === 'Aubert';     
    

